# Task histories

## Deployment - Part 2, Chapter 9

- build, push, release
```shell
$ docker build -f Dockerfile.prod -t registry.heroku.com/sheltered-river-27015/web .
$ docker push registry.heroku.com/sheltered-river-27015/web:latest
$ heroku container:release web --app sheltered-river-27015
```
- api
```shell
$ http https://sheltered-river-27015.herokuapp.com/api/movies/
```
```
$ http --json https://sheltered-river-27015.herokuapp.com/api/movies/
HTTP/1.1 200 OK
Allow: GET, POST, HEAD, OPTIONS
Connection: keep-alive
Content-Length: 466
Content-Type: application/json
Date: Sun, 25 Jul 2021 11:10:40 GMT
Referrer-Policy: same-origin
Server: gunicorn
Vary: Cookie
Via: 1.1 vegur
X-Content-Type-Options: nosniff
X-Frame-Options: DENY
```
```json
[
    {
        "created_date": "2021-01-07T14:07:13.540000Z",
        "genre": "comedy",
        "id": 1,
        "title": "Fargo",
        "updated_date": "2021-01-07T14:07:13.540000Z",
        "year": "1996"
    },
    {
        "created_date": "2021-01-07T14:06:59.408000Z",
        "genre": "thriller",
        "id": 2,
        "title": "No Country for Old Men",
        "updated_date": "2021-01-07T14:06:59.408000Z",
        "year": "2007"
    },
    {
        "created_date": "2021-01-07T14:06:51.542000Z",
        "genre": "comedy",
        "id": 3,
        "title": "A Serious Man",
        "updated_date": "2021-01-07T14:06:51.542000Z",
        "year": "2009"
    }
]
```
